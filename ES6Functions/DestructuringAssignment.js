export default () => {
    const [var1,,,var2] = ['val1', 'val2', 'val3', 'val4'];
    console.log(var1);
    console.log(var2);

    let vacation = {
        destination: 'Chile',
        travelers: 2,
        activity: 'skiing',
        cost: 4000
    }

    function vacationMarketing({destination, activity}) {
        return `Come to ${destination} and do some ${activity}`;
    }

    console.log(vacationMarketing(vacation));
}