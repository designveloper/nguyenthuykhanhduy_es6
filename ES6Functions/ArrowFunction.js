export default () => {
    let person = {
        first: 'Doug',
        actions: ['bike', 'hike', 'ski', 'surf'],
        printActions: function() {
            var _this = this;
            this.actions.forEach(function(action) {
                var str = _this.first + " likes to " + action;
                console.log(str);
            });

            this.actions.forEach(function(action) {
                var str = _this.first + " likes to " + action;
                console.log(str);
            }.bind(this));

            this.actions.forEach(action => {
                var str = _this.first + " likes to " + action;
                console.log(str);
            });
        }
    };

    person.printActions();
}