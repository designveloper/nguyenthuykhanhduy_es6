import LetKeyword from './ES6Syntax/LetKeyword';
import TemplateString from './ES6Syntax/TemplateString';
import SpreadOperator from './ES6Syntax/SpreadOperator';
import Map from './ES6Syntax/Map';
import Set from './ES6Syntax/Set';
import ForOfLoop from './ES6Syntax/ForOfLoop';
import ArrowFunction from './ES6Functions/ArrowFunction';
import DestructuringAssignment from './ES6Functions/DestructuringAssignment';
import Promise from './ AsynchronousFeatures/Promise';
import AsyncAwait from './ AsynchronousFeatures/AsyncAwait';
import Classes from './ES6Classes/Classes';

console.log('--- Let keyword ---')
LetKeyword();
console.log();

console.log('--- Template string ---')
TemplateString();
console.log();

console.log('--- Spread operator ---')
SpreadOperator();
console.log();

console.log('--- Map ---')
Map();
console.log();

console.log('--- Set ---')
Set();
console.log();

console.log('--- ForOfLoop ---')
ForOfLoop();
console.log();

console.log('--- ArrowFunction ---')
ArrowFunction();
console.log();

console.log('--- DestructuringAssignment ---')
DestructuringAssignment();
console.log();

console.log('--- Promise ---')
Promise();
console.log();

console.log('--- AsyncAwait ---')
AsyncAwait();
console.log();

console.log('--- Classes ---')
Classes();
console.log();