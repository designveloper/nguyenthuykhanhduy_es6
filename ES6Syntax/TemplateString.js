export default () => {
    const firstName = 'Duy Nguyen';
    console.log(`Hello ${firstName}`);

    const shipping = 5.95;
    const purchasePrice = 6.0;

    console.log(
        `Hi ${firstName}, thanks for buying from us!
            Total: $${purchasePrice}
            Shipping: $${shipping},
            Grand Total: $${purchasePrice + shipping}
        `)
}