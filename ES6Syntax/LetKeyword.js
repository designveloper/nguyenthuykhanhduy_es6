export default () => {
    var x = 10;

    if (x) {
        var x = 4;
    }

    console.log(x);

    var x = 10;

    if (x) {
        let x = 4;
    }

    console.log(x);
}