export default () => {
    let topics = new Map();
    topics.set('HTML', '/class/html');
    topics.set('CSS', '/class/css');
    topics.set('JavaScript', '/class/javascript');
    topics.set('Node', '/class/node');

    for (let topic of topics.keys()) {
        console.log(topic, 'is the course name');
    }

    for (let topic of topics.entries()) {
        console.log(topic);
    }
}