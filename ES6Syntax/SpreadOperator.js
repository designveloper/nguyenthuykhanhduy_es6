export default () => {
    let cats = ['cat1', 'cat2'];
    let dogs = ['dog1', 'dog2'];
    
    let animal = ['animal1', ...cats, 'animal2', ...dogs];

    console.log(animal);
}