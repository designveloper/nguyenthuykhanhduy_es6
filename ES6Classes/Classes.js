export default () => {
    class Vehicle {
        constructor(description, wheels) {
            this.description = description;
            this.wheels = wheels;
        }
        describeYourself() {
            console.log(`I am a ${this.description} with ${this.wheels} wheels`);
        }
    }
    var coolSkiVan = new Vehicle("cool ski van", 4);

    coolSkiVan.describeYourself();

    class SemiTruck extends Vehicle {
        constructor() {
            super("semi truck", 18)
        }
    }

    var groceryStoreSemi = new SemiTruck();
    groceryStoreSemi.describeYourself();

    var attendance = {
        _list: [],
        set addName(name) {
            this._list.push(name);
        },
        get list() {
            return this._list.join(', ');
        }
    };

    attendance.addName = 'Joanne';
    console.log("List getter:", attendance.list);
    console.log("Underscore list (prop)", attendance._list);

    attendance.addName = 'Anna';
    attendance.addName = 'Charlie';
    console.log(attendance.list);

    console.log("============= Classes ============== ")

    class Hike {
        constructor(distance, pace) {
            this.distance = distance;
            this.pace = pace;
        }
        get lengthInHours() {
            return `${this.calcLength()} hours`;
        }
        calcLength() {
            return this.distance / this.pace;
        }
    }

    const mtTallac = new Hike(10, 2);
    console.log(mtTallac.lengthInHours);
}