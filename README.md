# ECMAScript 6

## What is ES6

* ES6 is a standard that Javascript is based upon
* ES6 code needs to be transpiled in to ES5 before we try to render it in a browser
* Transpiling: 

    + Babel.js

    + Traceur

    + Closure

## Transpiling ES6

### In-browser Babel transpiling
* Add 
    ```
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.min.js"></script>
    ```
    
* Use 
    ```
    <script type="text/babel"></script>
    ```

### Transpiling with webpack

* Babel

    + ES6 code in -> ES5 code out

* Use webpack to automate the transpiling process

    + Webpack is a build tool that help us load all of our dependencies: CSS, images, node modules and more

* webpack configuration

```
const path = require('path');

module.exports = {
  entry: "./script.js",
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  }
}
```

### Map

* use something other than a string as a key
* utilize key value pairs that are always the same values
* need to iterate in order

### Sets

* Are collections of values
* Can be of any type
* Each value must be unique

### For of loop

* forEach: Performs given operation on each element of the array and return undefined

* map: Performs given "transformation" on a "copy" of each element and return new array with transformed elements

* for...of: can use with any iterable like map, set

### Arrow functions

* () => {} () (Arrow functions don't have their own this or arguments binding)

### Generators

```
function* aFunction() { yield; }

next() -> call next yield
.done -> true if finished
```