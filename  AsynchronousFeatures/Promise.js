import { XMLHttpRequest } from 'xmlhttprequest';

export default () => {
    const spacePeople = () => {
        return new Promise((resolves, rejects) => {
            const api = 'http://api.open-notify.org/astros.json';
            const request = new XMLHttpRequest();
            request.open('GET', api);
            request.onload = () => {
                if (request.status === 200) {
                    resolves(request.responseText);
                }
                else {
                    rejects(Error(request.statusText));
                }
            };
            request.onerror = err => rejects(err);
            request.send();
        });
    }

    spacePeople().then(
        spaceData => console.log(spaceData),
        err => console.log(new Error('Could not find space data'))
    );
}