import fetch from 'node-fetch';

export default () => {

    (async(loginName) => {
        try {
            let res = await fetch(`https://api.github.com/users/${loginName}/followers`);
            let json = await res.json();
            let followerList = json.map(user => user.login);
            console.log(followerList);
        }
        catch(e) {
            console.log('Data didnt load', e);
        }
    })('eveporcello');
}